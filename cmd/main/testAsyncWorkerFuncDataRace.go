package main

import (
	"fmt"

	"gitlab.com/evatix-go/asynchelper/async"
)

func testAsyncWorkerFuncDataRace(length int) {
	i := 0
	resp := async.Worker.IndexFuncProcessorDefault(
		length,
		func(index int) {
			fmt.Println("Index : ", index)
			i++
		},
	)

	resp.Wait()

	// Should give data race, if running with `go run -race cmd/main/*.go`
	fmt.Println(i)
}
