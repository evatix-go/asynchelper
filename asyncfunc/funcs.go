package asyncfunc

import (
	"sync"

	"gitlab.com/evatix-go/errorwrapper"
)

type (
	IndexProcessorFunc                    func(index int)
	IndexProcessorWithWaitGroupFunc       func(index int, wg *sync.WaitGroup)
	IndexProcessorReturnsErrorFunc        func(index int) error
	IndexProcessorReturnsErrorWrapperFunc func(index int) *errorwrapper.Wrapper
	AnyReturnFunc                         func() interface{}
	AnyWithErrorReturnFunc                func() (interface{}, error)
	VoidTask                              func()
)
