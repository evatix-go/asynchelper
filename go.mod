module gitlab.com/evatix-go/asynchelper

go 1.17

require (
	gitlab.com/evatix-go/core v1.3.55
	gitlab.com/evatix-go/errorwrapper v1.1.5
)

require (
	gitlab.com/evatix-go/enum v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
)
