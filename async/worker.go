package async

import (
	"sync"

	"gitlab.com/evatix-go/asynchelper/asyncfunc"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/errcore"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"
)

type worker struct{}

func (it *worker) IndexFuncProcessorDefault(
	length int,
	indexFunc asyncfunc.IndexProcessorFunc,
) *WorkerResponse {
	return it.IndexFuncProcessor(
		constants.SafeWorker,
		length,
		indexFunc)
}

func (it *worker) DebugIndexFuncProcessorDefault(
	length int,
	indexFunc asyncfunc.IndexProcessorFunc,
) *WorkerResponse {
	for i := 0; i < length; i++ {
		indexFunc(i)
	}

	return EmptyWorkerResponse()
}

func (it *worker) IndexFuncProcessor(
	workerCount,
	length int,
	indexFunc asyncfunc.IndexProcessorFunc,
) *WorkerResponse {
	if length == 0 {
		return EmptyWorkerResponse()
	}

	if length == 1 {
		indexFunc(0)

		return EmptyWorkerResponse()
	}

	if length <= workerCount {
		response := Index.FuncProcessor(
			length,
			indexFunc)

		return NewWorkerResponseUsingResponse(
			workerCount,
			nil,
			response)
	}

	wg := &sync.WaitGroup{}
	wg.Add(length)
	guard := make(chan struct{}, workerCount)

	runWrapper := func(index int) {
		indexFunc(index)

		<-guard
		wg.Done()
	}

	for i := 0; i < length; i++ {
		guard <- struct{}{}
		go runWrapper(i)
	}

	return &WorkerResponse{
		workerCount: workerCount,
		tasksLength: length,
		waitGroup:   wg,
		guard:       guard,
	}
}

func (it *worker) ReturnsErrorFuncProcessorDefault(
	length int,
	returnsErrorFunc asyncfunc.IndexProcessorReturnsErrorFunc,
) (*WorkerResponse, *errcore.RawErrCollection) {
	return it.ReturnsErrorFuncProcessor(
		constants.SafeWorker,
		length,
		returnsErrorFunc)
}

func (it *worker) ReturnsErrorFuncProcessor(
	workerCount,
	length int,
	returnsErrorFunc asyncfunc.IndexProcessorReturnsErrorFunc,
) (*WorkerResponse, *errcore.RawErrCollection) {
	if length == 0 {
		return EmptyWorkerResponse(), nil
	}

	rawErrCollection := &errcore.RawErrCollection{}

	if length == 1 {
		err := returnsErrorFunc(0)
		rawErrCollection.Add(err)

		return EmptyWorkerResponse(), rawErrCollection
	}

	wg := &sync.WaitGroup{}
	wg.Add(length)
	guard := make(chan struct{}, workerCount)
	locker := sync.Mutex{}

	runWrapper := func(index int) {
		err := returnsErrorFunc(index)

		if err != nil {
			locker.Lock()

			rawErrCollection.Add(err)

			locker.Unlock()
		}

		<-guard
		wg.Done()
	}

	for i := 0; i < length; i++ {
		guard <- struct{}{}
		go runWrapper(i)
	}

	return &WorkerResponse{
		workerCount: workerCount,
		tasksLength: length,
		waitGroup:   wg,
		guard:       guard,
	}, rawErrCollection
}

func (it *worker) ReturnsErrorWrapperFuncProcessor(
	workerCount,
	length int,
	returnsErrorWrapperFunc asyncfunc.IndexProcessorReturnsErrorWrapperFunc,
) (*WorkerResponse, *errwrappers.Collection) {
	if length == 0 {
		return EmptyWorkerResponse(), nil
	}

	errCollection := errwrappers.Empty()

	if length == 1 {
		errWp := returnsErrorWrapperFunc(0)

		return EmptyWorkerResponse(), errCollection.
			AddWrapperPtr(errWp)
	}

	wg := &sync.WaitGroup{}
	wg.Add(length)
	guard := make(chan struct{}, workerCount)
	locker := sync.Mutex{}

	runWrapper := func(index int) {
		err := returnsErrorWrapperFunc(index)

		if err != nil {
			locker.Lock()

			errCollection.AddWrapperPtr(err)

			locker.Unlock()
		}

		<-guard
		wg.Done()
	}

	for i := 0; i < length; i++ {
		guard <- struct{}{}
		go runWrapper(i)
	}

	return &WorkerResponse{
		workerCount: workerCount,
		tasksLength: length,
		waitGroup:   wg,
		guard:       guard,
	}, errCollection
}
