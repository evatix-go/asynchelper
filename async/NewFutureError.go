package async

import (
	"context"

	"gitlab.com/evatix-go/asynchelper/asyncfunc"
)

// NewFutureError
//
// executes the async function
//
// Reference : https://t.ly/hfxe
func NewFutureError(anyReturnFunc asyncfunc.AnyReturnFunc) ValueWithErrorAwaiter {
	var result interface{}
	c := make(chan struct{})

	go func() {
		defer close(c)

		result = anyReturnFunc()
	}()

	return futureWithError{
		await: func(ctx context.Context) (interface{}, error) {
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			case <-c:
				return result, nil
			}
		},
	}
}
