package async

import (
	"sync"

	"gitlab.com/evatix-go/asynchelper/asyncfunc"
	"gitlab.com/evatix-go/core/constants"
)

func TasksWithOnCompleteEvent(
	onCompleteEvent asyncfunc.VoidTask,
	tasks ...asyncfunc.VoidTask,
) *sync.WaitGroup {
	wg := TasksPlusWaitCount(
		constants.Zero,
		tasks...)

	finalWaitGroup := &sync.WaitGroup{}
	finalWaitGroup.Add(constants.One)

	onCompleteWrapperFunc := func() {
		wg.Wait()

		onCompleteEvent() // at the end
	}

	go onCompleteWrapperFunc()

	return finalWaitGroup
}
