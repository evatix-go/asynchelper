package async

import "sync"

type Response struct {
	tasksLength int
	waitGroup   *sync.WaitGroup
}

func EmptyResponse() *Response {
	return nil
}

func (it *Response) IsNoWaitRequired() bool {
	return it == nil || it.tasksLength <= 1 || it.waitGroup == nil
}

func (it *Response) IsRequiresWait() bool {
	return !it.IsNoWaitRequired()
}

func (it *Response) Wait() {
	if it.IsNoWaitRequired() {
		return
	}

	it.waitGroup.Wait()
}
