package async

// Awaiter interface has the method signature for await
type Awaiter interface {
	Await() interface{}
}
