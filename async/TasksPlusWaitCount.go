package async

import (
	"sync"

	"gitlab.com/evatix-go/asynchelper/asyncfunc"
)

// TasksPlusWaitCount
//
// Wait group is NOT called with wait.
// So developer needs to call it after when needs to be waited.
//
// Must call waitGroup.Wait() before working with data.
func TasksPlusWaitCount(
	additionalWaitCount int,
	tasks ...asyncfunc.VoidTask,
) *sync.WaitGroup {
	length := len(tasks)
	wg := &sync.WaitGroup{}
	wg.Add(additionalWaitCount)

	if length == 0 {
		return wg
	}

	wg.Add(length)

	if length == 1 {
		tasks[0]()
		wg.Done()

		return wg
	}

	runWrapper := func(index int) {
		tasks[index]()

		wg.Done()
	}

	for i := 0; i < length; i++ {
		go runWrapper(i)
	}

	return wg
}
