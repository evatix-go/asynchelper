package async

import "sync"

// WaitGroupJoiner
//
// It is async returns the final wait group.
//
// Doesn't wait or join auto.
// Developer must call finalWaitGroup.Wait() to be waited.
func WaitGroupJoiner(
	waitGroups ...*sync.WaitGroup,
) (finalWaitGroup *sync.WaitGroup) {
	length := len(waitGroups)
	if length == 0 {
		return
	}

	if length == 1 {
		waitGroups[0].Wait()

		return
	}

	finalWaitGroup = &sync.WaitGroup{}
	finalWaitGroup.Add(length)

	waitJoinerFunc := func(index int) {
		waitGroups[index].Wait()

		finalWaitGroup.Done()
	}

	for i := 0; i < length; i++ {
		go waitJoinerFunc(i)
	}

	return finalWaitGroup
}
