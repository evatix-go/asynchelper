package async

import (
	"sync"

	"gitlab.com/evatix-go/asynchelper/asyncfunc"
)

// TasksWaitGroup
//
// Wait group is NOT called with wait.
// So developer needs to call it after when needs to be waited.
//
// Must call waitGroup.Wait() before working with data.
func TasksWaitGroup(
	waitGroup *sync.WaitGroup,
	tasks ...asyncfunc.VoidTask,
) *sync.WaitGroup {
	length := len(tasks)
	if length == 0 {
		return waitGroup
	}

	waitGroup.Add(length)

	if length == 1 {
		tasks[0]()
		waitGroup.Done()

		return waitGroup
	}

	runWrapper := func(index int) {
		tasks[index]()

		waitGroup.Done()
	}

	for i := 0; i < length; i++ {
		go runWrapper(i)
	}

	return waitGroup
}
