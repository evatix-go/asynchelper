package async

import (
	"sync"

	"gitlab.com/evatix-go/asynchelper/asyncfunc"
	"gitlab.com/evatix-go/core/errcore"
)

type index struct{}

// FuncProcessor
//
// Doesn't wait tasks to be finished
//
// Developer must call the WaitGroup.Wait to be waited
func (it *index) FuncProcessor(
	length int,
	indexFunc asyncfunc.IndexProcessorFunc,
) *Response {
	wg := &sync.WaitGroup{}
	if length == 0 {
		return EmptyResponse()
	}

	if length == 1 {
		indexFunc(0)

		return EmptyResponse()
	}

	runWrapper := func(index int) {
		indexFunc(index)

		wg.Done()
	}

	wg.Add(length)

	for i := 0; i < length; i++ {
		go runWrapper(i)
	}

	return &Response{
		tasksLength: length,
		waitGroup:   wg,
	}
}

// ReturnsErrorFuncProcessor
//
// Doesn't wait tasks to be finished
//
// Developer must call the WaitGroup.Wait to be waited
func (it *index) ReturnsErrorFuncProcessor(
	length int,
	indexFunc asyncfunc.IndexProcessorReturnsErrorFunc,
) (*sync.WaitGroup, *errcore.RawErrCollection) {
	wg := &sync.WaitGroup{}

	if length == 0 {
		return wg, nil
	}

	rawErrCollection := errcore.RawErrCollection{}

	if length == 1 {
		err := indexFunc(0)
		rawErrCollection.Add(err)

		return wg, &rawErrCollection
	}

	locker := sync.Mutex{}

	runWrapper := func(index int) {
		defer wg.Done()
		err := indexFunc(index)

		if err != nil {
			locker.Lock()

			rawErrCollection.Add(err)

			locker.Unlock()
		}
	}

	wg.Add(length)

	for i := 0; i < length; i++ {
		go runWrapper(i)
	}

	return wg, &rawErrCollection
}

// WaitedGroupFuncProcessor
//
// Developer must call wg.Done on each index func processor done
//
// Doesn't wait tasks to be finished
//
// Developer must call the WaitGroup.Wait to be waited
func (it *index) WaitedGroupFuncProcessor(
	length int,
	indexFunc asyncfunc.IndexProcessorWithWaitGroupFunc,
) *sync.WaitGroup {
	wg := &sync.WaitGroup{}
	if length == 0 {
		return wg
	}

	wg.Add(length)

	if length == 1 {
		indexFunc(0, wg)

		return wg
	}

	for i := 0; i < length; i++ {
		go indexFunc(i, wg)
	}

	return wg
}
