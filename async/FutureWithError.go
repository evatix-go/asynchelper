package async

import (
	"context"
)

type futureWithError struct {
	await func(ctx context.Context) (interface{}, error)
}

func (f futureWithError) Await() (interface{}, error) {
	return f.await(context.Background())
}
