package syncparallel

import (
	"gitlab.com/evatix-go/errorwrapper/errfunc"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"
)

func TasksWithErrorWrapperCollect(
	isContinueOnError bool,
	errorCollection *errwrappers.Collection,
	tasks ...errfunc.WrapperFunc,
) (isSuccess bool) {
	length := len(tasks)
	if length == 0 {
		return true
	}

	if length == 1 {
		errWp := tasks[0]()

		errorCollection.AddWrapperPtr(errWp)

		return errWp.IsSuccess()
	}

	generatedErrCollection := TasksWithErrorWrapperReturnsErrorCollection(
		isContinueOnError,
		tasks...)

	if errorCollection.HasError() {
		errorCollection.AddCollections(generatedErrCollection)

		return false
	}

	return true
}
