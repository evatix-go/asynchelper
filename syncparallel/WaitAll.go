package syncparallel

import "sync"

func WaitAll(
	waitGroups ...*sync.WaitGroup,
) {
	length := len(waitGroups)
	if length == 0 {
		return
	}

	if length == 1 {
		waitGroups[0].Wait()

		return
	}

	finalWaitGroup := sync.WaitGroup{}
	finalWaitGroup.Add(length)

	waitJoinerFunc := func(index int) {
		waitGroups[index].Wait()

		finalWaitGroup.Done()
	}
	for i := 0; i < length; i++ {
		go waitJoinerFunc(i)
	}

	finalWaitGroup.Wait()
}
